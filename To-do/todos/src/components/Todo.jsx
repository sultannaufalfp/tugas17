import React, {Component} from "react";
import "./Todo.css";
import ToDoItem from "./ToDoItem";

class Todo extends Component{
    render() {
        return (
            <div className="todo-list">
            <ToDoItem />
        </div>
        )
    }
}

export default Todo;