import React, {Component} from "react";
import "./ToDoItem.css";

class ToDoItem extends Component{
    constructor(props){
        super(props)
    }
    render() {
        return(
            <div className="container todo-item">
            <input type="checkbox" />
            <p>{this.props.name}</p>
        </div>
        )
    }
}

export default ToDoItem;