const ToDoData = [
    {
        id: 1,
        name: "Take out the trash",
        completed: true
    },
    {
        id: 2,
        name: "Grocery shopping",
        completed: false
    },
    {
        id: 3,
        name: "Clean the dish",
        completed: false
    },
    {
        id: 4,
        name: "Mow lawn",
        completed: true
    },
    {
        id: 5,
        name: "Catch up on Arrested Development",
        completed: false
    }
]

export default ToDoData;