import './App.css';
import ToDoData from "./components/ToDoData";
import ToDoItem from "./components/ToDoItem";

function App() {
  let data = ToDoData
  return (
        <body>
          {data.map(dat => {
            return(<ToDoItem name={dat.name}/>)
          })}
        </body>
  );
}

export default App;
